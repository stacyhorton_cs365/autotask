<?php
/**
 * @author David Van Ronk
 * @copyright Netcomm Systems LLC.
 * @version 20120219.400
 */

namespace AutoTask;



class AutoTask

{
	private $accounts;
	private $resources;
	private $tickets;
	private $roles;
	private $ticketStatusList;
	// private $webservicesurl = "https://webservices2.autotask.net/atservices/1.5/";
	private $webservicesurl;
	private $username;
	private $password;
	private $roleid;
	private $at = "";
	public $client;
	const TKSTATUS_COMPLETE = 5;

	/**
	 * __construct function sets up the autotask connection parameters
	 * @param string $pusername      The AutoTask Username to use
	 * @param string $ppassword      The AutoTask Password to use
	 * @param string $webservicesurl The AutoTask URL to use
	 * @author David Van Ronk
	 *
	 */
	function __construct($pusername, $ppassword, $webservicesurl = "") {
		$this->username = $pusername;
		$this->password = $ppassword;
		if ($webservicesurl != "") {
			$this->webservicesurl = $webservicesurl;
		}

	}

	/**
	 * connectToAPI() Sets up the ATWS proxy class to connect and then loads static values.
	 * @author David Van Ronk
	 */
	function connectToAPI() {
		if ( ! $this->webservicesurl ) {
			$webservicesurl = 'https://webservices.autotask.net/atservices/1.5/';
			$wsdl = $webservicesurl . 'atws.wsdl';
			$loginarray = array ('login' => $this->username, 'password' => $this->password, 'uri' => $webservicesurl, 'location' => $webservicesurl . "atws.asmx" );
			$client = new ATWS ($wsdl, $loginarray);
			$response = $client->getZoneInfo($this->username);
			// error_log(__METHOD__.'(): response = '.var_export($response, true));
			$this->webservicesurl = str_replace('atws.asmx', '', $response->getZoneInfoResult->URL);
		}
		$wsdl = $this->webservicesurl . 'atws.wsdl';
		$loginarray = array ('login' => $this->username, 'password' => $this->password, 'uri' => $this->webservicesurl, 'location' => $this->webservicesurl . "atws.asmx" );
		$this->client = new ATWS ( $wsdl, $loginarray );

		return $this->webservicesurl;
	}


	public function getWebServicesUrl () {
		return $this->webservicesurl;
	}

	/**
	 * loadAccounts() loads all accounts
	 */
	function loadAccounts() {
		$xml = '

		<queryxml>
		<entity>Account</entity>
		<query>
		<field>id<expression op="greaterthan">-1</expression></field>
		</query>
		</queryxml>

		';
		$query = new query ( $xml );

		$result = $this->client->query ( $query );
		$this->accounts = Array ();

		$laccounts = $result->queryResult->EntityResults->Entity;
		foreach ( $laccounts as $account ) {
			$this->accounts [$account->id] = $account;
		}

	}

	/**
	 * loadResources() loads all  Resource Entities into the  Resources array
	 * @todo optimize this by setting the id in the array index
	 */
	function loadResources() {

		$query = new query ( '

				<queryxml>
				<entity>Resource</entity>
				<query>
				<field>id<expression op="greaterthan">0</expression></field>
				</query>
				</queryxml>
				' );
		$result = $this->client->query ( $query );

		$this->resources = $result->queryResult->EntityResults->Entity;

		return $this->resources;
	}

	/**
	 * loadRoles() loads all  Resource Entities into the  Resources array
	 * @todo optimize this by setting the id in the array index
	 * @return array of Roles
	 */
	function loadRoles() {

		$query = new query ( '

				<queryxml>
				<entity>Role</entity>
				<query>
				<field>id<expression op="greaterthan">0</expression></field>
				</query>
				</queryxml>
				' );
		$result = $this->client->query ( $query );

		$this->roles = $result->queryResult->EntityResults->Entity;

		return $this->roles;
	}

	/**
	 * loadTickets() Loads all ticket from autotask that are not the given status
	 * @param int $NotStatus
	 */
	function loadTickets($NotStatus = Autotask::TKSTATUS_COMPLETE)  {
		$this->tickets = Array ();

		$xml = "<queryxml>
		<entity>Ticket</entity>" . "
		<query>
		<field>Status
		<expression op='NotEqual'>" . $NotStatus . "</expression>
		</field>
		</query>
		</queryxml>";

		$result = $this->client->query ( new query ($xml) );
		$unsortedobj = $result->queryResult->EntityResults->Entity;
		foreach ( $unsortedobj as $object ) {
			$this->tickets [$object->TicketNumber] = $object;
		}

	}

	/**
	 * loadPickLists() loads all pick list values into the associated object arrays
	 */
	function loadPickLists() {
		$parameters = new GetFieldInfo ();
		$parameters->psObjectType = "Ticket";
		$obj = $this->client->GetFieldInfo ( $parameters );
		$this->ticketStatusList = Array ();

		foreach ( $obj as $o ) {

			foreach ( $o->Field as $field ) {

				if ($field->Name == 'Status') {

					$this->ticketStatusList = $field->PicklistValues->PickListValue;
				}
			}
		}

	}

	/**
	 *  getLoggedinResource() Returns the logged in Resource
	 * @return Resource the currently logged in resource
	 */
	function getLoggedinResource() {
		$this->loadResources ();

		foreach ( $this->resources as $resource ) {
			if (strtolower ( $resource->Email ) == strtolower ( $this->username )) {
				return $resource;
			}
		}
	}

	/**
	 * setDirty() Clears data that might be changed by a update or create
	 */
	function setDirty() {

		unset ( $this->tickets );
	}

	/**
	 * getTicket returns the specified ticket, if nothing is found a null is returned.
	 * @param string $number The Ticket Number needed
	 * @return Ticket
	 * @author David Van Ronk
	 */
	function getTicket($number) {
		if(isset($this->tickets [$number])){
			return $this->tickets [$number];
		}else{
			//This is odd but lets try reloading the tickets from AT
			$this->loadTickets(AutoTask::TKSTATUS_COMPLETE);
		}
		//Lets see if the ticket we needed is here now
		if(isset($this->tickets [$number])){
			return $this->tickets [$number];
		}
		//No ticket matching was found so just return a new ticket
		return new Ticket();
		;
	}


	/**
	 * getTickets() returns a Array of Ticket Entities
	 * @return Array of Ticket
	 */
	function getTickets(){
		if(!isset($this->tickets)){
			$this->loadTickets(AutoTask::TKSTATUS_COMPLETE);
		}
		return $this->tickets;
	}


	/**
	 * getTicketStatusList() returns all the ticket statuses that are loaded, if non are loaded it loads them
	 * @return array of PickListValue:
	 */
	function getTicketStatusList() {
		if (! isset ( $this->ticketStatusList )) {
			$this->loadPickLists ();
		}

		return $this->ticketStatusList;
	}

	/**
	 * getTicketStatus() returns a ticket status value object
	 * @param int $status
	 * @return PickListValue
	 */
	function getTicketStatus($status) {

		if ($status > 0) {
			foreach ( $this->ticketStatusList as $choice ) {
				if ($choice ['value'] == $status) {
					return $choice;
				}
			}
		}
		return new PickListValue();
	}

	/**
	 * getAccount() Gets the requested Account or returns a new account
	 * @param int $accountid Account ID Number
	 * @return Account
	 */
	function getAccount($accountid) {
		if($accountid >= 0){
		return $this->accounts [$accountid];
		}else{
			return new Account();
		}
	}


	/**
	 * getResource() Returns the resource with the specified ID
	 * @param int $id Resource ID
	 * @return Resource
	 */
	function getResource($id) {

		if ($id > 0) {
			foreach ( $this->resources as $resource ) {
				if ($resource->id == $id) {
					return $resource;
				}
			}
		}

		return new Resource();

	}

	/**
	 * loadProducts() Loads all products from autotask
	 */
	function loadProducts ()  {
		$this->products = array();

		$xml = "<queryxml>
		<entity>Product</entity>" . "
			<query>
				<field>Active
				<expression op='Equals'>true</expression>
				</field>
			</query>
		</queryxml>";

		$result = $this->client->query ( new query ($xml) );
		error_log(__METHOD__.'(): result = '.var_export($result, true));
		$unsortedobj = $result->queryResult->EntityResults->Entity;
		foreach ( $unsortedobj as $object ) {
			$this->products [$object->id] = $object;
		}

	}


	/**
	 * getProducts() returns a Array of Product Entities
	 * @return Array of Products
	 */
	function getProducts () {
		if ( ! isset($this->products) ) {
			$this->loadProducts();
		}
		return $this->products;
	}


	/**
	 * loadAllocationCodes() Loads all Allocation Codes from autotask
	 */
	function loadAllocationCodes ()  {
		$this->allocation_codes = array();
				// <field>Name
				// 	<expression op='BeginsWith'>Managed</expression>
				// </field>


		$xml = "<queryxml>
		<entity>AllocationCode</entity>
			<query>
				<field>id
					<expression op='IsNotNull'></expression>
				</field>
			</query>
		</queryxml>";

		$result = $this->client->query ( new query ($xml) );
		error_log(__METHOD__.'(): result = '.var_export($result, true));
		$unsortedobj = $result->queryResult->EntityResults->Entity;
		foreach ( $unsortedobj as $object ) {
			$this->allocation_codes [$object->id] = $object;
		}

	}


	/**
	 * getAllocationCodes() returns a Array of AllocationCode Entities
	 * @return Array of Products
	 */
	function getAllocationCodes () {
		if ( ! isset($this->allocation_codes) ) {
			$this->loadAllocationCodes();
		}
		return $this->allocation_codes;
	}


	function getEntities ( $entity, $filters = null ) {

		if ( ! $filters ) {
			$filters = array( array( 'id', 'IsNotNull', '' ) );
		}

		$filter_str = '';
		foreach ( $filters as $filter ) {
			$filter_str .= sprintf('<field>%s<expression op="%s">%s</expression></field>', $filter[0], $filter[1], $filter[2]);
		}

		$xml = "<queryxml><entity>${entity}</entity><query>${filter_str}</query></queryxml>";
		// error_log(__METHOD__.'(): xml = '.var_export($xml, true));

		$result = $this->client->query ( new query ($xml) );
		// error_log(__METHOD__.'(): result = '.var_export($result, true));
		$result_array = array();
		if ( isset($result->queryResult->EntityResults->Entity) ) {
			$unsortedobj = $result->queryResult->EntityResults->Entity;
			if ( is_object($unsortedobj) ) {
				$unsortedobj = array( $unsortedobj );
			}
			foreach ( $unsortedobj as $object ) {
				if ( is_object($object) && $object !== null ) {
					// $result_array[$object->id] = $object;
					$result_array[] = $object;
				}
			}
		}

		return $result_array;
	}

	function getPicklistValues ( $entity_name, $field_name ) {
		$result = $this->client->getFieldInfo($entity_name);
		if ( $result && isset($result->GetFieldInfoResult->Field) ) {
			$found = false;
			$picklist = array();

			foreach ( $result->GetFieldInfoResult->Field as $field ) {
				if ( $field->Name == $field_name ) {
					$found = true;
					if ( isset($field->IsPickList) ) {
						$picklist = $field->PicklistValues->PickListValue;
					} else {
						throw new \RuntimeException("'".$field_name."' is not a Picklist field");
					}
				}
			}

			if ( $found ) {
				return $picklist;
			} else {
				throw new \InvalidArgumentException("No field called '".$field_name."'");
			}
		}
		throw new \RuntimeException('No response from API');
	}


	public function getATWS () {
		return $this->client;
	}


	public function createInstalledProduct ( $attributes ) {

		$defaults = array(
			'id' => 0,
			'Active' => true,
			'InstallDate' => date('c'),
		);

		$installed_product = new InstalledProduct(array_merge($defaults, $attributes));

		if ( isset($attributes['UserDefinedFields']) ) {
			$installed_product->UserDefinedFields = array();
			foreach ( $attributes['UserDefinedFields'] as $key => $value ) {
				$udf = new UserDefinedField($key, $value);
				$installed_product->UserDefinedFields[] = $udf;
			}
		}

		$result = $this->client->create($installed_product);

		if ( $result->createResult->ReturnCode != 1 ) {
			$ex = new APIException($result->createResult->Errors->ATWSError->Message);
			$ex->ATWSResponse = $result;
			throw $ex;
		}

		$autotask_entity = $result->createResult->EntityResults->Entity;

		return $autotask_entity;
	}


	public function updateInstalledProduct ( $installed_product_id, $attributes ) {
		$installed_products = $this->getEntities('InstalledProduct', array( array( 'id', 'Equals', $installed_product_id)));
		if ( $installed_products ) {
			sort($installed_products);
			$installed_product = $installed_products[0];
			// error_log(__METHOD__.'(): installed_product = '.var_export($installed_product, true));
			foreach ( $attributes as $key => $value ) {
				if ( $key == 'Notes' ) {
					$installed_product->Notes .= ( $installed_product->Notes ? "\n\n" : '' ).$value;
				} elseif ( $key == 'UserDefinedFields' ) {

					foreach ( $value as $udf_name => $udf_value ) {
						foreach ( $installed_product->UserDefinedFields->UserDefinedField as $udf ) {
							if ( $udf->Name == $udf_name ) {
								$udf->Value = $udf_value;
								continue;
							}
						}
					}

				} else {
					$installed_product->{$key} = $value;
				}
			}

			// @note Autotask API hates empty strings, set them to null
			if ( ! $installed_product->Location ) {
				$installed_product->Location = null;
			}
			if ( ! $installed_product->SerialNumber ) {
				$installed_product->SerialNumber = null;
			}
			// error_log(__METHOD__.'(): installed_product = '.var_export($installed_product, true));
			$result = $this->client->update($installed_product);
			if ( $result->updateResult->ReturnCode !== 1 ) {
				$ex = new \Autotask\APIException($result->updateResult->Errors->ATWSError->Message);
				$ex->ATWSResponse = $result;
				throw $ex;
				// error_log(__METHOD__.'(): result = '.var_export($result, true));
				// return false;
			}
			return $result;
		}

		return null;
	}

}